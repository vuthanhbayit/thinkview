/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  theme: {
    fontFamily: {
      sans: ['Quarion', ...defaultTheme.fontFamily.sans]
    },
    container: {
      center: true,
      padding: {
        default: '1rem'
      }
    },
    extend: {
      colors: {
        primary: '#27CEBE',
        info: '#042261',
        dark: '#011235',
        pearl: '#03182d',
        gray: {
          dark: '#080D1B',
          default: '#4B5161',
          medium: '#878B96',
          blue: '#DEE1E9',
          '100': '#C3C5CB',
          '200': '#EFF0F4',
          '300': '#F7F9FB'
        }
      }
    }
  },
  variants: {},
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js'
    ]
    // These options are passed through directly to PurgeCSS
    // options: {
    //   whitelist: ['bg-red-500', 'px-4']
    // }
  }
}
